import csv
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
import time

arquivoCSV = open('4678.csv')
leitor = csv.reader(arquivoCSV)
dados = np.array(list(leitor))

tempo = dados[:,1].astype(np.float)
eixoX = dados[:,2].astype(np.float)
eixoY = dados[:,3].astype(np.float)
eixoZ = dados[:,4].astype(np.float)
eixos = [eixoX, eixoY, eixoZ]
nomes = ['eixo X', 'eixo Y', 'eixo Z']

for i in range(3):
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(eixos[i])
    plt.title('Plot do ' + nomes[i] + ' no tempo e na frequência')
    plt.ylabel('Tempo')

    # Número de amostras
    N = len(eixos[i])
    # Período de amostragem
    T = 1.0 / 800.0     # 800 Hz ; T = 1/800
    yf = scipy.fftpack.fft(eixos[i])
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

    plt.subplot(2, 1, 2)
    plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.ylabel('Frequência')
    plt.show(block=False)
