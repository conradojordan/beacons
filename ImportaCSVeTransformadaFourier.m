% Conrado Viveiros Jordan - Byond, 2018
% Realização da Transformada de Fourier de um sinal importado de um arquivo .csv



%%%%%%%%%%%%%%%% Importação do csv (gerado pelo Matlab) %%%%%%%%%%%%%%%%%

% Initialize variables.
filename = '4678.csv';
delimiter = ',';

% Read columns of data as strings:
formatSpec = '%*q%q%q%q%q%[^\n\r]';

% Open the text file.
fileID = fopen(filename,'r');

% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);

% Close the text file.
fclose(fileID);

% Convert the contents of columns containing numeric strings to numbers.
% Replace non-numeric strings with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,3,4]
    % Converts strings in the input cell array to numbers. Replaced non-numeric
    % strings with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(thousandsRegExp, ',', 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric strings to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
        end
    end
end

% Replace non-numeric cells with NaN
R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),raw); % Find non-numeric cells
raw(R) = {NaN}; % Replace non-numeric cells

% Allocate imported array to column variable names
tempo = cell2mat(raw(:, 1));
eixoX = cell2mat(raw(:, 2));
eixoY = cell2mat(raw(:, 3));
eixoZ = cell2mat(raw(:, 4));

% Clear temporary variables
clearvars filename delimiter formatSpec fileID dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp me R;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Transformada de Fourier


% Variável com as medidas de vibração
dados = {'Eixo X', 'Eixo Y', 'Eixo Z'; eixoX, eixoY, eixoZ};


Fs = 800;       % Frequência de amostragem                   
T = 1/Fs;       % Período de amostragem 

for var = 1:length(dados)
    nomeEixo = dados{1,var};
    eixoAtual = dados{2,var};
    L = length(eixoAtual);       % Tamanho do sinal
    t = (0:L-1)*T;          % Vetor tempo
    f = Fs*(0:(L/2))/L;     % Vetor frequencia

    % Gráfico no tempo do sinal
    h = figure
    plot(1000*t,eixoAtual)
    title(sprintf('X(t) - %s',nomeEixo))
    xlabel('Tempo (millissegundos)')
    ylabel('X(t)')
    set(h,'WindowStyle','docked')

    % FFT do sinal
    Y = fft(eixoAtual);
    P2 = abs(Y/L);
    P1 = P2(1:L/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    
    % Gráfico da FFT
    g = figure
    plot(f,P1) 
    title(sprintf('Espectro de amplitude na frequência do %s',nomeEixo))
    xlabel('frequência (Hz)')
    ylabel('|F(w)|')
    set(g,'WindowStyle','docked')
end

% Deleção das variáveis temporárias
clear eixoAtual nomeEixo var dados;